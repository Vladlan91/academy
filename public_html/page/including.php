
 <?php
 echo '<div class="social_link">
              <ul class="sociallink_nav">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
              </ul>
            </div>
            <div class="related_post">
              <h2><i class="fa fa-thumbs-o-up"></i></h2>
              <ul class="spost_nav wow fadeInDown animated">
                <li>
               <div class="media"> <a class="media-left" href="/loads/material/id/2"> <img src="../images/featured_img3.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/2"> Зверь по имени «Лин Шесть сигм»</a> </div>
                  </div>
                </li>
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/7"> <img src="../images/featured_img2.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/7"> Внедрение Лин Шесть сигм в Министерстве обороны США.</a> </div>
                  </div>
                </li>
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/8"> <img src="../images/featured_img1.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/8"> Опыт внедрения технологии Шесть сигм в NAVSEA.</a> </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <nav class="nav-slit"> <a class="prev" href="#"> <span class="icon-wrap"><i class="fa fa-angle-left"></i></span>
        <div>
          <h3>City Lights</h3>
          <img src="../images/post_img1.jpg" alt=""/> </div>
        </a> <a class="next" href="#"> <span class="icon-wrap"><i class="fa fa-angle-right"></i></span>
        <div>
          <h3>Street Hills</h3>
          <img src="../images/post_img1.jpg" alt=""/> </div>
        </a> </nav>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <aside class="right_content">
          <div class="single_sidebar">
            <h2><span>популярные темы</span></h2>
            <ul class="spost_nav">
              <li>
                <div class="single_post_content_left">
              <ul class="business_catgnav  wow fadeInDown">
                <li>
                  <figure class="bsbig_fig"> <a href="/loads/material/id/2" class="featured_img"> <img alt="" src="images/featured_img3.jpg"> <span class="overlay"></span> </a>
                    <figcaption> <a href="/loads/material/id/2">Зверь по имени «Лин Шесть сигм»</a> </figcaption>
                    <p>В постиндустриальной экономике в результате ускорения научно-технического прогресса, взрывного роста номенклатуры продукции, и пер...</p>
                  </figure>
                </li>
              </ul>
            </div>
                <div class="single_post_content_right">
              <ul class="business_catgnav  wow fadeInDown">
                <li>
                  <figure class="bsbig_fig"> <a href="/loads/material/id/3" class="featured_img"> <img alt="" src="images/featured_img5.jpg"> <span class="overlay"></span> </a>
                    <figcaption> <a href="/loads/material/id/2">Как вовлечь сотрудников в Lean.</a> </figcaption>
                    <p>Важным моментом внедрения Бережливого производства на предприятии является создание среды для изменения мышлени...</p>
                  </figure>
                </li>
              </ul>
            </div>
            </ul>
          </div>
          <div class="single_sidebar">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#category" aria-controls="home" role="tab" data-toggle="tab">Темы</a></li>
              <li role="presentation"><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Видео</a></li>
              <li role="presentation"><a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Форум</a></li>
            </ul>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="category">
                <ul>
                  <li class="cat-item"><a href="#">ПЕРСОНАЛ</a></li>
                  <li class="cat-item"><a href="#">АКАДЕМИЯ SIX SIGMA</a></li>
                  <li class="cat-item"><a href="#">БЕРЕЖЛИВОЕ ПРОИЗВОДСТВО</a></li>
                  <li class="cat-item"><a href="#">SIX SIGMA?</a></li>
                  <li class="cat-item"><a href="#">ПРОЕКТЫ SIX SIGMA</a></li>
                  <li class="cat-item"><a href="#">АВТОМАТИЗАЦИЯ ПРОИЗВОДСТВА</a></li>
                  <li class="cat-item"><a href="#">ЧЛЕНСТВО</a></li>
                </ul>
              </div>
              <div role="tabpanel" class="tab-pane" id="video">
                <div class="vide_area">
                  <iframe width="100%" height="250" src="http://www.youtube.com/embed/h5QWbURNEpA?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="comments">
                <ul class="spost_nav">
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 1</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 2</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 3</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 4</a> </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="single_sidebar wow fadeInDown">
            <h2><span>Спонсор</span></h2>
            <a class="sideAdd" href="#"><img src="images/add_img.jpg" alt=""></a> </div>
        </aside>
      </div>
    </div>'
    ?>