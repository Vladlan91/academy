<?php Head('') ?>
<body>
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
<?php MessageShow(); ?>
<?php MenuOne(); ?>
<?php MenuTwo(); ?>
  <section id="newsSection">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="latest_newsarea"> <span>Latest News</span>
          <ul id="ticker01" class="news_sticker">
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Инструменты бережливого производства и их сущность.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Оценка влияет на мотивацию: О ежегодной оценке рабочих.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Премия как инструмент для мотивации.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Поставщикам открывают Дорожную карту.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Процессно-ориентированное управление в Caterpillar.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Как повысить эффективность с помощью ИТ-систем.</a></li>
          </ul>
          <div class="social_area">
            <ul class="social_nav">
              <li class="facebook"><a href="#"></a></li>
              <li class="twitter"><a href="#"></a></li>
              <li class="flickr"><a href="#"></a></li>
              <li class="pinterest"><a href="#"></a></li>
              <li class="googleplus"><a href="#"></a></li>
              <li class="vimeo"><a href="#"></a></li>
              <li class="youtube"><a href="#"></a></li>
              <li class="mail"><a href="#"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="sliderSection">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="slick_slider">
          <div class="single_iteam"> <a href="/sixsigma"> <img src="images/slider_img4.jpg" alt=""></a>
            <div class="slider_article">
              <h2><a class="slider_tittle" href="/sixsigma">SIX SIGMA?</a></h2>
              <p>В настоящее время лидерами являются три системы: Лин, Шесть сигм и Теория ограничений Гольдратта как наиболее стройные и самодостаточные системы совершенствования деятельности предприятий, которые гарантируют получение высокого экономического эффекта при внедрении....</p>
            </div>
          </div>
          <div class="single_iteam"> <a href="/leanproduction"> <img src="images/slider_img2.jpg" alt=""></a>
            <div class="slider_article">
              <h2><a class="slider_tittle" href="/leanproduction">Lean Production?</a></h2>
              <p>Lean — концепция менеджмента основана на неуклонном стремлении к устранению всех видов потерь. Отправная точка оценки затрат в бережливом производстве — ценность для потребителя...</p>
            </div>
          </div>
          <div class="single_iteam"> <a href="/toc"> <img src="images/slider_img3.jpg" alt=""></a>
            <div class="slider_article">
              <h2><a class="slider_tittle" href="/toc">TOC?</a></h2>
              <p>Теория ограничений — популярная методология управления производством, разработанная в 1980-е годы Элияху Голдраттом, в основе которой лежит нахождение и управление ключевым огр...</p>
            </div>
          </div>
          <div class="single_iteam"> <a href=""> <img src="images/slider_img1.jpg" alt=""></a>
            <div class="slider_article">
              <h2><a class="slider_tittle" href="">Производственные системы!</a></h2>
              <p>В постиндустриальной экономике в результате ускорения научно-технического прогресса, взрывного роста номенклатуры продукции, и перехода от массового к по-заказному производству...</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="latest_post">
          <h2><span>Последние комментарии</span></h2>
          <div class="latest_post_container">
            <div id="prev-button"><i class="fa fa-chevron-up"></i></div>
            <ul class="latest_postnav">
              <li>
                <div class="media"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/photograph_img7.jpg"> </a>
                  <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 1</a> </div>
                </div>
              </li>
              <li>
                <div class="media"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/photograph_img8.jpg"> </a>
                  <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 2</a> </div>
                </div>
              </li>
              <li>
                <div class="media"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/photograph_img9.jpg"> </a>
                  <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 3</a> </div>
                </div>
              </li>
              <li>
                <div class="media"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/photograph_img10.jpg"> </a>
                  <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 4</a> </div>
                </div>
              </li>
              <li>
                <div class="media"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/photograph_img11.jpg"> </a>
                  <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 2</a> </div>
                </div>
              </li>
            </ul>
            <div id="next-button"><i class="fa  fa-chevron-down"></i></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="contentSection">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="left_content">
          <div class="single_post_content">
            <h2><span>производственные системы</span></h2>
            <div class="single_post_content_left">
              <ul class="business_catgnav  wow fadeInDown">
                <li>
                  <figure class="bsbig_fig"> <a href="/loads/material/id/4" class="featured_img"> <img alt="" src="images/featured_img6.jpg"> <span class="overlay"></span> </a>
                    <figcaption> <a href="/loads/material/id/4">Выстраивание непрерывного поточного производства.</a> </figcaption>
                    <p>Одной из важных тенденций развития современного производства является повышение эффективности производственной системы за счет ритмичности от...</p>
                  </figure>
                </li>
              </ul>
            </div>
               <div class="single_post_content_right">
              <ul class="business_catgnav  wow fadeInDown">
                <li>
                  <figure class="bsbig_fig"> <a href="/loads/material/id/5" class="featured_img"> <img alt="" src="images/featured_img7.jpg"> <span class="overlay"></span> </a>
                    <figcaption> <a href="/loads/material/id/5">Инструменты бережливого производства и их сущность.</a> </figcaption>
                    <p>Бережливое производство (система Lean) – это особый подход к организации управления на предприятии. Он направлен на улучшение качества работы ...</p>
                  </figure>
                </li>
              </ul>
            </div>
          </div>
          <div class="fashion_technology_area">
            <div class="fashion">
              <div class="single_post_content">
                <h2><span>ПЕРСОНАЛ</span></h2>
                <ul class="business_catgnav wow fadeInDown">
                  <li>
                    <figure class="bsbig_fig"> <a href="/loads/material/id/6" class="featured_img"> <img alt="" src="images/featured_img4.jpg"> <span class="overlay"></span> </a>
                      <figcaption> <a href="/loads/material/id/6">Программа подготовки кадрового резерва.</a> </figcaption>
                      <p>Двухнедельный цикл обучения участников программы созыва 2016 года на производственных площадках ...</p>
                    </figure>
                  </li>
                </ul>
                <ul class="spost_nav">
                  <li>
                    <div class="media wow fadeInDown"> <a href="/loads/material/id/1" class="media-left"> <img alt="" src="images/photograph_img2.jpg"> </a>
                      <div class="media-body"> <a href="/loads/material/id/1" class="catg_title">JuniorSkills-2017: Чемпионат на современных станках.</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="/loads/material/id/10" class="media-left"> <img alt="" src="images/photograph_img3.jpg"> </a>
                      <div class="media-body"> <a href="/loads/material/id/10" class="catg_title">Оценка влияет на мотивацию: О ежегодной оценке рабочих.</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="/loads/material/id/11" class="media-left"> <img alt="" src="images/zap2.jpg"> </a>
                      <div class="media-body"> <a href="/loads/material/id/11" class="catg_title">Премия как инструмент для мотивации.</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="/loads/material/id/12" class="media-left"> <img alt="" src="images/zap1.jpg"> </a>
                      <div class="media-body"> <a href="/loads/material/id/12" class="catg_title">Премия за производительность.</a> </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="technology">
              <div class="single_post_content">
                <h2><span>АВТОМАТИЗАЦИЯ</span></h2>
                <ul class="business_catgnav">
                  <li>
                    <figure class="bsbig_fig wow fadeInDown"> <a href="/loads/material/id/9" class="featured_img"> <img alt="" src="images/featured_img8.jpg"> <span class="overlay"></span> </a>
                      <figcaption> <a href="/loads/material/id/9">Как повысить эффективность с помощью ИТ-систем.</a> </figcaption>
                      <p>Ключевую роль в этом играют информационные бизнес-системы, которые успешно внедряются на предприятиях холдинга на протяжении после...</p>
                    </figure>
                  </li>
                </ul>
                <ul class="spost_nav">
                  <li>
                    <div class="media wow fadeInDown"> <a href="/loads/material/id/13" class="media-left"> <img alt="" src="images/it1.jpg"> </a>
                      <div class="media-body"> <a href="/loads/material/id/13" class="catg_title">Экспертное мнение. Что такое цифровое производство?</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="/loads/material/id/14" class="media-left"> <img alt="" src="images/it2.jpg"> </a>
                      <div class="media-body"> <a href="/loads/material/id/14" class="catg_title">Первые результаты работы технопарка промышленной автоматизации.</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="/loads/material/id/15" class="media-left"> <img alt="" src="images/it3.png"> </a>
                      <div class="media-body"> <a href="/loads/material/id/15" class="catg_title"> Как система мониторинга оборудования может изменить производство?</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="/loads/material/id/16" class="media-left"> <img alt="" src="images/it4.jpeg"> </a>
                      <div class="media-body"> <a href="/loads/material/id/16" class="catg_title">Оптимизация планирования. Что дает умное производство?</a> </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="single_post_content">
            <h2><span>Kонференции LEAN</span></h2>
            <ul class="photograph_nav  wow fadeInDown">
              <li>
                <div class="photo_grid">
                  <figure class="effect-layla"> <a class="fancybox-buttons" data-fancybox-group="button" href="images/photograph_img1.jpg" title="Photography Ttile 1"> <img src="images/photograph_img1.jpg" alt=""/></a> </figure>
                </div>
              </li>
              <li>
                <div class="photo_grid">
                  <figure class="effect-layla"> <a class="fancybox-buttons" data-fancybox-group="button" href="images/photograph_img2.jpg" title="Photography Ttile 2"> <img src="images/photograph_img2.jpg" alt=""/> </a> </figure>
                </div>
              </li>
              <li>
                <div class="photo_grid">
                  <figure class="effect-layla"> <a class="fancybox-buttons" data-fancybox-group="button" href="images/photograph_img3.jpg" title="Photography Ttile 3"> <img src="images/photograph_img3.jpg" alt=""/> </a> </figure>
                </div>
              </li>
              <li>
                <div class="photo_grid">
                  <figure class="effect-layla"> <a class="fancybox-buttons" data-fancybox-group="button" href="images/photograph_img4.jpg" title="Photography Ttile 4"> <img src="images/photograph_img4.jpg" alt=""/> </a> </figure>
                </div>
              </li>
              <li>
                <div class="photo_grid">
                  <figure class="effect-layla"> <a class="fancybox-buttons" data-fancybox-group="button" href="images/photograph_img5.jpg" title="Photography Ttile 5"> <img src="images/photograph_img5.jpg" alt=""/> </a> </figure>
                </div>
              </li>
              <li>
                <div class="photo_grid">
                  <figure class="effect-layla"> <a class="fancybox-buttons" data-fancybox-group="button" href="images/photograph_img6.jpg" title="Photography Ttile 6"> <img src="images/photograph_img6.jpg" alt=""/> </a> </figure>
                </div>
              </li>
            </ul>
          </div>
          <div class="single_post_content">
            <h2><span>ПРОЕКТЫ SIX SIGMA</span></h2>
            <div class="single_post_content_left">
              <ul class="business_catgnav">
                <li>
                  <figure class="bsbig_fig  wow fadeInDown"> <a class="featured_img" href="/loads/material/id/8"> <img src="images/featured_img1.jpg" alt=""> <span class="overlay"></span> </a>
                    <figcaption> <a href="/loads/material/id/8">Опыт внедрения технологии Шесть сигм в NAVSEA.</a> </figcaption>
                    <p>NAVSEA – это крупнейшее командование в структуре ВМФ США, отвечающее за капитальный ремонт, обслуживание и модернизацию военно...</p>
                  </figure>
                </li>
              </ul>
            </div>
               <div class="single_post_content_right">
              <ul class="business_catgnav">
                <li>
                  <figure class="bsbig_fig  wow fadeInDown"> <a class="featured_img" href="/loads/material/id/7"> <img src="images/featured_img2.jpg" alt=""> <span class="overlay"></span> </a>
                    <figcaption> <a href="/loads/material/id/7">Внедрение Лин Шесть сигм в Министерстве обороны США.</a> </figcaption>
                    <p>Внедрение «Шести сигм» в американском военно-промышленном комплексе стартовало в начале 2000-х с ряда отдельных проектов и локаль...</p>
                  </figure>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <aside class="right_content">
          <div class="single_sidebar">
            <h2><span>популярные темы</span></h2>
            <ul class="spost_nav">
              <li>
                <div class="single_post_content_left">
              <ul class="business_catgnav  wow fadeInDown">
                <li>
                  <figure class="bsbig_fig"> <a href="/loads/material/id/2" class="featured_img"> <img alt="" src="images/featured_img3.jpg"> <span class="overlay"></span> </a>
                    <figcaption> <a href="pages/single_page.html">Зверь по имени «Лин Шесть сигм»</a> </figcaption>
                    <p>В постиндустриальной экономике в результате ускорения научно-технического прогресса, взрывного роста номенклатуры продукции, и пер...</p>
                  </figure>
                </li>
              </ul>
            </div>
                <div class="single_post_content_right">
              <ul class="business_catgnav  wow fadeInDown">
                <li>
                  <figure class="bsbig_fig"> <a href="/loads/material/id/3" class="featured_img"> <img alt="" src="images/featured_img5.jpg"> <span class="overlay"></span> </a>
                    <figcaption> <a href="pages/single_page.html">Как вовлечь сотрудников в Lean.</a> </figcaption>
                    <p>Важным моментом внедрения Бережливого производства на предприятии является создание среды для изменения мышлени...</p>
                  </figure>
                </li>
              </ul>
            </div>
            </ul>
          </div>
          <div class="single_sidebar">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#category" aria-controls="home" role="tab" data-toggle="tab">Темы</a></li>
              <li role="presentation"><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Видео</a></li>
              <li role="presentation"><a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Форум</a></li>
            </ul>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="category">
                <ul>
                  <li class="cat-item"><a href="#">ПЕРСОНАЛ</a></li>
                  <li class="cat-item"><a href="#">АКАДЕМИЯ SIX SIGMA</a></li>
                  <li class="cat-item"><a href="#">БЕРЕЖЛИВОЕ ПРОИЗВОДСТВО</a></li>
                  <li class="cat-item"><a href="#">SIX SIGMA?</a></li>
                  <li class="cat-item"><a href="#">ПРОЕКТЫ SIX SIGMA</a></li>
                  <li class="cat-item"><a href="#">АВТОМАТИЗАЦИЯ ПРОИЗВОДСТВА</a></li>
                  <li class="cat-item"><a href="#">ЧЛЕНСТВО</a></li>
                </ul>
              </div>
              <div role="tabpanel" class="tab-pane" id="video">
                <div class="vide_area">
                  <iframe width="100%" height="250" src="http://www.youtube.com/embed/h5QWbURNEpA?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="comments">
                <ul class="spost_nav">
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 1</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 2</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 3</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 4</a> </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="single_sidebar wow fadeInDown">
            <h2><span>Спонсор</span></h2>
            <a class="sideAdd" href="#"><img src="images/add_img.jpg" alt=""></a> </div>
          <div class="single_sidebar wow fadeInDown">
            <h2><span>категории новостей</span></h2>
            <select class="catgArchive">
              <option>Select Category</option>
              <option>Life styles</option>
              <option>Sports</option>
              <option>Technology</option>
              <option>Treads</option>
            </select>
          </div>
          <div class="single_sidebar wow fadeInDown">
            <h2><span>ссылки на книги</span></h2>
            <ul>
              <li><a href="#">Six Sigma</a></li>
              <li><a href="#">Lean Production</a></li>
              <li><a href="#">DAO Toyota</a></li>
              <li><a href="#">Lean Six Sigma</a></li>
            </ul>
          </div>
        </aside>
      </div>
    </div>
  </section>
  <?php Footer() ?>
</div>
</body>
</html>