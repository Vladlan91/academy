<?php 
Head('') ?>
<body>
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
<?php  MessageShow();?>
<?php MenuOne(); ?>
<?php MenuTwo(); ?>
   <section id="newsSection">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="latest_newsarea"> <span>Последние новости</span>
          <ul id="ticker01" class="news_sticker">
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Инструменты бережливого производства и их сущность.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Оценка влияет на мотивацию: О ежегодной оценке рабочих.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Премия как инструмент для мотивации.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Поставщикам открывают Дорожную карту.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Процессно-ориентированное управление в Caterpillar.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Как повысить эффективность с помощью ИТ-систем.</a></li>
          </ul>
          <div class="social_area">
            <ul class="social_nav">
              <li class="facebook"><a href="#"></a></li>
              <li class="twitter"><a href="#"></a></li>
              <li class="flickr"><a href="#"></a></li>
              <li class="pinterest"><a href="#"></a></li>
              <li class="googleplus"><a href="#"></a></li>
              <li class="vimeo"><a href="#"></a></li>
              <li class="youtube"><a href="#"></a></li>
              <li class="mail"><a href="#"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="contentSection">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="left_content">
          <div class="single_page">
            <ol class="breadcrumb">
              <li><a href="../index.html">Home</a></li>
              <li><a href="#">News</a></li>
              <li class="active">Sixsigma</li>
            </ol>
            <h1>Sixsigma</h1>
            <div class="post_commentbox"> <a href="#"><i class="fa fa-user"></i>Admin</a> <span><i class="fa fa-calendar"></i>26.05.2017 6:49 AM</span> <a href="#"><i class="fa fa-tags"></i>Sixsigma</a> </div>
               <div class="single_page_content"> <img class="img-center" src="../images/sixsigmafoto.jpg" alt="">
              <p>Шесть сигм (англ. six sigma) — концепция управления производством, разработанная в корпорации Motorola в 1986 году и популяризированная в середине 1990-х после того, как Джек Уэлч применил её как ключевую стратегию в General Electric. Суть концепции сводится к необходимости улучшения качества выходов каждого из процессов, минимизации дефектов и статистических отклонений в операционной деятельности. Концепция использует методы управления качеством, в том числе, статистические методы, требует использования измеримых целей и результатов, а также предполагает создание специальных рабочих групп на предприятии, осуществляющих проекты по устранению проблем и совершенствованию процессов («чёрные пояса», «зелёные пояса»).</p>
              <blockquote> Название происходит от статистического понятия среднеквадратичного отклонения, обозначаемого греческой буквой σ. Зрелость производственного процесса в этой концепции описывается как σ-рейтинг отклонений, или процентом бездефектной продукции на выходе, так, процесс управления качеством 6σ на выходе даёт 99,99966 % выходов без дефектов, или не более 3,4 дефектных выходов на 1 млн операций. Motorola установила в качестве цели достижение показателя качества 6σ для всех производственных процессов, и именно этот уровень и дал наименование концепции. </blockquote>
              <h3>Базовые принципы:</h3>
              <ul>
                <li>Для успешного ведения бизнеса необходимо постоянно стремиться к установлению устойчивого и предсказуемого протекания процессов;</li>
                <li>Показатели (KPI), характеризующие протекание процессов производства и бизнес-процессов, должны быть измеряемыми, контролируемыми и улучшаемыми, а также отражать изменения в протекании процессов;</li>
                <li>Для достижения постоянного улучшения качества необходимо вовлечение персонала организации на всех уровнях, особенно высшего руководства.</li>
              </ul>
              <h3>Также указываются следующие базовые принципы:</h3>
               <ul>
                <li>искренний интерес к клиенту;</li>
                <li>управление на основе данных и фактов;</li>
                <li>ориентированность на процесс, управление процессом и совершенствование процесса; </li>
                <li>проактивное (упреждающее) управление;</li>
                <li>взаимодействие без границ (прозрачность внутрикорпоративных барьеров);</li>
                <li>стремление к совершенству плюс снисходительность к неудачам;</li>
              </ul>
              <br> <blockquote> При реализации проектов совершенствования процессов часто используется последовательность этапов DMAIC (англ. define, measure, analyze, improve, control): </blockquote> 
               <ul>
                <li>определение целей проекта и запросов потребителей (внутренних и внешних);</li>
                <li>измерение процесса, чтобы определить текущее выполнение;</li>
                <li>анализ и определение коренных причин дефектов;</li>
                <li>улучшение процесса, сокращающего дефекты;</li>
                <li>контроль дальнейшего протекания процесса.</li>
               </ul>
               <br>
            </div>
             <div class="social_link">
              <ul class="sociallink_nav">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
              </ul>
            </div>
            <div class="related_post">
              <h2><i class="fa fa-thumbs-o-up"></i></h2>
              <ul class="spost_nav wow fadeInDown animated">
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/2"> <img src="../images/featured_img3.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/2"> Зверь по имени «Лин Шесть сигм»</a> </div>
                  </div>
                </li>
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/7"> <img src="../images/featured_img2.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/7"> Внедрение Лин Шесть сигм в Министерстве обороны США.</a> </div>
                  </div>
                </li>
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/8"> <img src="../images/featured_img1.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/8"> Опыт внедрения технологии Шесть сигм в NAVSEA.</a> </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <nav class="nav-slit"> <a class="prev" href="#"> <span class="icon-wrap"><i class="fa fa-angle-left"></i></span>
        <div>
          <h3>City Lights</h3>
          <img src="../images/post_img1.jpg" alt=""/> </div>
        </a> <a class="next" href="#"> <span class="icon-wrap"><i class="fa fa-angle-right"></i></span>
        <div>
          <h3>Street Hills</h3>
          <img src="../images/post_img1.jpg" alt=""/> </div>
        </a> </nav>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <aside class="right_content">
          <div class="single_sidebar">
            <h2><span>популярные темы</span></h2>
            <ul class="spost_nav">
              <li>
                <div class="single_post_content_left">
              <ul class="business_catgnav  wow fadeInDown">
                <li>
                  <figure class="bsbig_fig"> <a href="/loads/material/id/2" class="featured_img"> <img alt="" src="images/featured_img3.jpg"> <span class="overlay"></span> </a>
                    <figcaption> <a href="/loads/material/id/2">Зверь по имени «Лин Шесть сигм»</a> </figcaption>
                    <p>В постиндустриальной экономике в результате ускорения научно-технического прогресса, взрывного роста номенклатуры продукции, и пер...</p>
                  </figure>
                </li>
              </ul>
            </div>
                <div class="single_post_content_right">
              <ul class="business_catgnav  wow fadeInDown">
                <li>
                  <figure class="bsbig_fig"> <a href="/loads/material/id/3" class="featured_img"> <img alt="" src="images/featured_img5.jpg"> <span class="overlay"></span> </a>
                    <figcaption> <a href="/loads/material/id/3">Как вовлечь сотрудников в Lean.</a> </figcaption>
                    <p>Важным моментом внедрения Бережливого производства на предприятии является создание среды для изменения мышлени...</p>
                  </figure>
                </li>
              </ul>
            </div>
            </ul>
          </div>
          <div class="single_sidebar">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#category" aria-controls="home" role="tab" data-toggle="tab">Темы</a></li>
              <li role="presentation"><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Видео</a></li>
              <li role="presentation"><a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Форум</a></li>
            </ul>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="category">
                <ul>
                  <li class="cat-item"><a href="#">ПЕРСОНАЛ</a></li>
                  <li class="cat-item"><a href="#">АКАДЕМИЯ SIX SIGMA</a></li>
                  <li class="cat-item"><a href="#">БЕРЕЖЛИВОЕ ПРОИЗВОДСТВО</a></li>
                  <li class="cat-item"><a href="#">SIX SIGMA?</a></li>
                  <li class="cat-item"><a href="#">ПРОЕКТЫ SIX SIGMA</a></li>
                  <li class="cat-item"><a href="#">АВТОМАТИЗАЦИЯ ПРОИЗВОДСТВА</a></li>
                  <li class="cat-item"><a href="#">ЧЛЕНСТВО</a></li>
                </ul>
              </div>
              <div role="tabpanel" class="tab-pane" id="video">
                <div class="vide_area">
                  <iframe width="100%" height="250" src="http://www.youtube.com/embed/h5QWbURNEpA?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="comments">
                <ul class="spost_nav">
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 1</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 2</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 3</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 4</a> </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="single_sidebar wow fadeInDown">
            <h2><span>Спонсор</span></h2>
            <a class="sideAdd" href="#"><img src="images/add_img.jpg" alt=""></a> </div>
        </aside>
      </div>
    </div>
  </section>
<?php Footer() ?>
</div>
</body>
</html>
