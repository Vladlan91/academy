<?php 
Head('') ?>
<body>
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
<?php  MessageShow();?>
<?php MenuOne(); ?>
<?php MenuTwo(); ?>
   <section id="newsSection">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="latest_newsarea"> <span>Latest News</span>
          <ul id="ticker01" class="news_sticker">
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Инструменты бережливого производства и их сущность.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Оценка влияет на мотивацию: О ежегодной оценке рабочих.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Премия как инструмент для мотивации.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Поставщикам открывают Дорожную карту.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Процессно-ориентированное управление в Caterpillar.</a></li>
            <li><a href="#"><img src="images/news_thumbnail3.jpg" alt="">Как повысить эффективность с помощью ИТ-систем.</a></li>
          </ul>
          <div class="social_area">
            <ul class="social_nav">
              <li class="facebook"><a href="#"></a></li>
              <li class="twitter"><a href="#"></a></li>
              <li class="flickr"><a href="#"></a></li>
              <li class="pinterest"><a href="#"></a></li>
              <li class="googleplus"><a href="#"></a></li>
              <li class="vimeo"><a href="#"></a></li>
              <li class="youtube"><a href="#"></a></li>
              <li class="mail"><a href="#"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
   <section id="contentSection">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="left_content">
          <div class="single_page">
            <ol class="breadcrumb">
              <li><a href="../index.html">Home</a></li>
              <li><a href="#">News</a></li>
              <li class="active">Lean Production</li>
            </ol>
            <h1>Lean Production</h1>
            <div class="post_commentbox"> <a href="#"><i class="fa fa-user"></i>Admin</a> <span><i class="fa fa-calendar"></i>26.05.2017 6:49 AM</span> <a href="#"><i class="fa fa-tags"></i>Lean Production</a> </div>
               <div class="single_page_content"> <img class="img-center" src="../images/leanproduction.jpg" alt="">
              <p>Бережли́вое произво́дство (от англ. lean production, lean manufacturing — «стройное производство») — концепция управления производственным предприятием, основанная на постоянном стремлении к устранению всех видов потерь. Бережливое производство предполагает вовлечение в процесс оптимизации бизнеса каждого сотрудника и максимальную ориентацию на потребителя. Возникла как интерпретация идей производственной системы компании Toyota американскими исследователями её феномена.</p>
              <h3>Основные аспекты:</h3>
              <blockquote> Отправная точка концепции — оценка ценности продукта для конечного потребителя, на каждом этапе его создания. В качестве основной задачи предполагается создание процесса непрерывного устранения потерь, то есть устранение любых действий, которые потребляют ресурсы, но не создают ценности (не являются важными) для конечного потребителя. В качестве синонима для понятия потерь иногда используется термин из производственной системы Toyota — muda (яп. 無駄 муда), означающий всевозможные затраты, потери, отходы, мусор. Например, потребителю совершенно не нужно, чтобы готовый продукт или его детали лежали на складе. Тем не менее, при традиционной системе управления складские издержки, а также все расходы, связанные с переделкой, браком, и другие косвенные издержки перекладываются на потребителя.
В соответствии с концепцией бережливого производства, вся деятельность предприятия делится на операции и процессы, добавляющие ценность для потребителя, и операции и процессы, не добавляющие ценности для потребителя. Задачей «бережливого производства» является планомерное сокращение процессов и операций, не добавляющих ценности.</blockquote>
              <h3>Виды потерь:</h3>
              <p>Тайити Оно (1912—1990), один из главных создателей производственной системы компании Toyota, выделил 7 видов потерь:</p>
               <ul>
                <li>потери из-за перепроизводства;</li>
                <li>потери времени из-за ожидания;</li>
                <li>потери при ненужной транспортировке;</li>
                <li>потери из-за лишних этапов обработки;</li>
                <li>потери из-за лишних запасов;</li>
                <li>потери из-за ненужных перемещений;</li>
                <li>потери из-за выпуска дефектной продукции.</li>
              </ul>
              <br><p>Тайити Оно считал перепроизводство основным видом потерь, в результате которых возникают остальные. Джеффри Лайкер, исследователь производственной системы Toyota (наряду с Джеймсом Вумеком и Дэниелом Джонсом), в книге «Дао Тойота» добавил ещё один вид потерь:</p>
              <ul>
                <li>нереализованный творческий потенциал сотрудников.</li>
              </ul>
             <p>Также принято выделять ещё два источника потерь — muri (яп. 無理 му́ри), — перегрузка рабочих, сотрудников или мощностей при работе с повышенной интенсивностью и mura (яп. 斑 му́ра) — неравномерность выполнения операции, например, прерывистый график работ из-за колебаний спроса.</p>
              <h3>Основные принципы:</h3>
              <p>Джеймс Вумек и Дэниел Джонс в книге «Бережливое производство: Как избавиться от потерь и добиться процветания вашей компании» излагают суть бережливого производства как процесс, который включает пять этапов:</p>
               <ul>
                <li>Определить ценность конкретного продукта.</li>
                <li>Определить поток создания ценности для этого продукта.</li>
                <li>Обеспечить непрерывное течение потока создания ценности продукта.</li>
                <li>Позволить потребителю вытягивать продукт.</li>
                <li>Стремиться к совершенству.</li>
              </ul>
            </div >
              <div class="related_post">
              <h2><i class="fa fa-thumbs-o-up"></i></h2>
              <ul class="spost_nav wow fadeInDown animated">
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/2"> <img src="../images/featured_img3.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/2"> Зверь по имени «Лин Шесть сигм»</a> </div>
                  </div>
                </li>
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/7"> <img src="../images/featured_img2.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/7"> Внедрение Лин Шесть сигм в Министерстве обороны США.</a> </div>
                  </div>
                </li>
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/8"> <img src="../images/featured_img1.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/8"> Опыт внедрения технологии Шесть сигм в NAVSEA.</a> </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <nav class="nav-slit"> <a class="prev" href="#"> <span class="icon-wrap"><i class="fa fa-angle-left"></i></span>
        <div>
          <h3>City Lights</h3>
          <img src="../images/post_img1.jpg" alt=""/> </div>
        </a> <a class="next" href="#"> <span class="icon-wrap"><i class="fa fa-angle-right"></i></span>
        <div>
          <h3>Street Hills</h3>
          <img src="../images/post_img1.jpg" alt=""/> </div>
        </a> </nav>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <aside class="right_content">
          <div class="single_sidebar">
            <h2><span>популярные темы</span></h2>
            <ul class="spost_nav">
              <li>
                <div class="single_post_content_left">
              <ul class="business_catgnav  wow fadeInDown">
                <li>
                  <figure class="bsbig_fig"> <a href="/loads/material/id/2" class="featured_img"> <img alt="" src="images/featured_img3.jpg"> <span class="overlay"></span> </a>
                    <figcaption> <a href="/loads/material/id/2">Зверь по имени «Лин Шесть сигм»</a> </figcaption>
                    <p>В постиндустриальной экономике в результате ускорения научно-технического прогресса, взрывного роста номенклатуры продукции, и пер...</p>
                  </figure>
                </li>
              </ul>
            </div>
                <div class="single_post_content_right">
              <ul class="business_catgnav  wow fadeInDown">
                <li>
                  <figure class="bsbig_fig"> <a href="/loads/material/id/3" class="featured_img"> <img alt="" src="images/featured_img5.jpg"> <span class="overlay"></span> </a>
                    <figcaption> <a href="/loads/material/id/3">Как вовлечь сотрудников в Lean.</a> </figcaption>
                    <p>Важным моментом внедрения Бережливого производства на предприятии является создание среды для изменения мышлени...</p>
                  </figure>
                </li>
              </ul>
            </div>
            </ul>
          </div>
          <div class="single_sidebar">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#category" aria-controls="home" role="tab" data-toggle="tab">Темы</a></li>
              <li role="presentation"><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Видео</a></li>
              <li role="presentation"><a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Форум</a></li>
            </ul>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="category">
                <ul>
                  <li class="cat-item"><a href="#">ПЕРСОНАЛ</a></li>
                  <li class="cat-item"><a href="#">АКАДЕМИЯ SIX SIGMA</a></li>
                  <li class="cat-item"><a href="#">БЕРЕЖЛИВОЕ ПРОИЗВОДСТВО</a></li>
                  <li class="cat-item"><a href="#">SIX SIGMA?</a></li>
                  <li class="cat-item"><a href="#">ПРОЕКТЫ SIX SIGMA</a></li>
                  <li class="cat-item"><a href="#">АВТОМАТИЗАЦИЯ ПРОИЗВОДСТВА</a></li>
                  <li class="cat-item"><a href="#">ЧЛЕНСТВО</a></li>
                </ul>
              </div>
              <div role="tabpanel" class="tab-pane" id="video">
                <div class="vide_area">
                  <iframe width="100%" height="250" src="http://www.youtube.com/embed/h5QWbURNEpA?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="comments">
                <ul class="spost_nav">
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 1</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 2</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 3</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 4</a> </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="single_sidebar wow fadeInDown">
            <h2><span>Спонсор</span></h2>
            <a class="sideAdd" href="#"><img src="images/add_img.jpg" alt=""></a> </div>
        </aside>
      </div>
    </div>
  </section>
<?php Footer() ?>
</div>
</body>
</html>
