<?php 
ULogin(0);
Head('') ?>
<body>
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
<?php  MessageShow();?>
<?php MenuOne(); ?>
<?php MenuTwo(); ?>
 <section id="newsSection">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="latest_newsarea"> <span>Latest News</span>
          <ul id="ticker01" class="news_sticker">
            <li><a href="#"><img src="../images/news_thumbnail3.jpg" alt="">My First News Item</a></li>
            <li><a href="#"><img src="../images/news_thumbnail3.jpg" alt="">My Second News Item</a></li>
            <li><a href="#"><img src="../images/news_thumbnail3.jpg" alt="">My Third News Item</a></li>
            <li><a href="#"><img src="../images/news_thumbnail3.jpg" alt="">My Four News Item</a></li>
            <li><a href="#"><img src="../images/news_thumbnail3.jpg" alt="">My Five News Item</a></li>
            <li><a href="#"><img src="../images/news_thumbnail3.jpg" alt="">My Six News Item</a></li>
            <li><a href="#"><img src="../images/news_thumbnail3.jpg" alt="">My Seven News Item</a></li>
            <li><a href="#"><img src="../images/news_thumbnail3.jpg" alt="">My Eight News Item</a></li>
            <li><a href="#"><img src="../images/news_thumbnail2.jpg" alt="">My Nine News Item</a></li>
          </ul>
          <div class="social_area">
            <ul class="social_nav">
              <li class="facebook"><a href="#"></a></li>
              <li class="twitter"><a href="#"></a></li>
              <li class="flickr"><a href="#"></a></li>
              <li class="pinterest"><a href="#"></a></li>
              <li class="googleplus"><a href="#"></a></li>
              <li class="vimeo"><a href="#"></a></li>
              <li class="youtube"><a href="#"></a></li>
              <li class="mail"><a href="#"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="contentSection">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="left_content">
           <form method="POST" action="/account/login">
                <div class="loginpanel">
                <div class="txt">
                  <input type="text" name="login" placeholder="Логин" maxlength="10" pattern="[A-Za-z-0-9]{3,10}" title="Не менее 3 и неболее 10 латынских символов или цифр." required>
                  <label for="user" class="entypo-user"></label>
                </div>
                <div class="txt">
                  <input type="password" name="password" placeholder="Пароль" maxlength="15"  pattern="[A-Za-z-0-9]{5,15}" title="Не менее 5 и неболее 15 латынских символов или цифр." required>
                  <label for="pwd" class="entypo-lock"></label>
                </div>
                <div class="buttons">
                  <input type="submit" class="btn btn-orange" name="enter" value="Вход"><br> <br><input type="reset" class="btn btn-orange" value="Очистить">
                  <span>
                    <a href="/register" class="entypo-user-add register">Register</a>
                  </span>
                </div>
                <div class="txt">
                    <div class="capdiv"><input type="text" class="capinp" name="captcha" maxlength="10" pattern="[0-9]{1,5}" title="Только цифры." required> <img src="/resource/captcha.php" class="capimg" alt="Каптча"></div>
                 </div>
                <div class="hr">
                  <div></div>
                  <div>OR</div>
                  <div></div>
                </div>
                <div class="social">
                  <a href="" class="facebook"></a>
                  <a href="" class="twitter"></a>
                  <a href="" class="googleplus"></a>
                </div>
              </div>
            </form>
            <div class="related_post">
              <h2><i class="fa fa-thumbs-o-up"></i></h2>
              <ul class="spost_nav wow fadeInDown animated">
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/2"> <img src="../images/featured_img3.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/2"> Зверь по имени «Лин Шесть сигм»</a> </div>
                  </div>
                </li>
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/7"> <img src="../images/featured_img2.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/7"> Внедрение Лин Шесть сигм в Министерстве обороны США.</a> </div>
                  </div>
                </li>
                <li>
                  <div class="media"> <a class="media-left" href="/loads/material/id/8"> <img src="../images/featured_img1.jpg" alt=""> </a>
                    <div class="media-body"> <a class="catg_title" href="/loads/material/id/8"> Опыт внедрения технологии Шесть сигм в NAVSEA.</a> </div>
                  </div>
                </li>
              </ul>
            </div>
              <div class="social_link">
              <ul class="sociallink_nav">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
              </ul>
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <aside class="right_content">
          <div class="single_sidebar">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#category" aria-controls="home" role="tab" data-toggle="tab">Темы</a></li>
              <li role="presentation"><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Видео</a></li>
              <li role="presentation"><a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Форум</a></li>
            </ul>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="category">
                <ul>
                  <li class="cat-item"><a href="#">ПЕРСОНАЛ</a></li>
                  <li class="cat-item"><a href="#">АКАДЕМИЯ SIX SIGMA</a></li>
                  <li class="cat-item"><a href="#">БЕРЕЖЛИВОЕ ПРОИЗВОДСТВО</a></li>
                  <li class="cat-item"><a href="#">SIX SIGMA?</a></li>
                  <li class="cat-item"><a href="#">ПРОЕКТЫ SIX SIGMA</a></li>
                  <li class="cat-item"><a href="#">АВТОМАТИЗАЦИЯ ПРОИЗВОДСТВА</a></li>
                  <li class="cat-item"><a href="#">ЧЛЕНСТВО</a></li>
                </ul>
              </div>
              <div role="tabpanel" class="tab-pane" id="video">
                <div class="vide_area">
                  <iframe width="100%" height="250" src="http://www.youtube.com/embed/h5QWbURNEpA?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="comments">
                <ul class="spost_nav">
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 1</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 2</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img1.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 3</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="images/post_img2.jpg"> </a>
                      <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 4</a> </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="single_sidebar wow fadeInDown">
            <h2><span>Спонсор</span></h2>
            <a class="sideAdd" href="#"><img src="images/add_img.jpg" alt=""></a> </div>
          <div class="single_sidebar wow fadeInDown">
            <h2><span>категории новостей</span></h2>
            <select class="catgArchive">
              <option>Select Category</option>
              <option>Life styles</option>
              <option>Sports</option>
              <option>Technology</option>
              <option>Treads</option>
            </select>
          </div>
          <div class="single_sidebar wow fadeInDown">
            <h2><span>ссылки на книги</span></h2>
            <ul>
              <li><a href="#">Six Sigma</a></li>
              <li><a href="#">Lean Production</a></li>
              <li><a href="#">DAO Toyota</a></li>
              <li><a href="#">Lean Six Sigma</a></li>
            </ul>
          </div>
        </aside>
      </div>
    </div>
  </section>
<?php Footer() ?>
</div>
</body>
</html>