<?php 
$Param['page'] += 0;
$Param['cat'] += 0;

if ($Param['cat'] and $Param['cat'] <= 0 or $Param['cat'] > 3) MessageSend(1, 'Такой категории не существует.', '/news');

Head('Каталог файлов');
?>
<body>
<div class="container">
<?php  MessageShow();?>
<?php MenuOne(); ?>
<?php MenuTwo(); ?>
  <section id="contentSection">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="left_content">
           
<div class="single_page">
            <ol class="breadcrumb">
<?php if ($_SESSION['USER_LOGIN_IN']) echo '<li><a href="/loads/add">Добавить новость</a></li>' ?>
<li><a href="/loads">Все новости</a></li>
<li><a href="/loads/main/cat/1">Персонал</a></li>
<li><a href="/loads/main/cat/2">Управления проектами</a></li>
<li><a href="/loads/main/cat/3">Автоматизация</a></li>
</ol>

<?php 
if ($Module == 'main' and !$Param['cat']) {
if ($_SESSION['USER_GROUP'] != 2) $Active = 'WHERE `active` = 1';
$Param1 = 'SELECT `id`, `name`, `added`, `date`, `text`, `active`, `dimg`, `dfile`, `link`, `rate`, `rateusers`FROM `loads` '.$Active.' ORDER BY `id` DESC LIMIT 0, 5';
$Param2 = 'SELECT `id`, `name`, `added`, `date`, `text`, `active`, `dimg`, `dfile`, `link`, `rate`, `rateusers`FROM `loads` '.$Active.' ORDER BY `id` DESC LIMIT START, 5';
$Param3 = 'SELECT COUNT(`id`) FROM `loads`';
$Param4 = '/loads/main/page/';
} else {
if ($_SESSION['USER_GROUP'] != 2) $Active = 'AND `active` = 1';
$Param1 = 'SELECT `id`, `name`, `added`, `date`, `text`, `active`, `dimg`, `dfile`, `link`, `rate`, `rateusers`FROM `loads` WHERE `cat` = '.$Param['cat'].' '.$Active.' ORDER BY `id` DESC LIMIT 0, 5';
$Param2 = 'SELECT `id`, `name`, `added`, `date`, `text`, `active`, `dimg`, `dfile`, `link`, `rate`, `rateusers`FROM `loads` WHERE `cat` = '.$Param['cat'].' '.$Active.' ORDER BY `id` DESC LIMIT START, 5';
$Param3 = 'SELECT COUNT(`id`) FROM `loads` WHERE `cat` = '.$Param['cat'];
$Param4 = '/loads/main/cat/'.$Param['cat'].'/page/';
}

$Count = mysqli_fetch_row(mysqli_query($CONNECT, $Param3));

if (!$Param['page']) {
$Param['page'] = 1;
$Result = mysqli_query($CONNECT, $Param1);
} else {
$Start = ($Param['page'] - 1) * 5;
$Result = mysqli_query($CONNECT, str_replace('START', $Start, $Param2));
}


PageSelector($Param4, $Param['page'], $Count);

while ($Row = mysqli_fetch_assoc($Result)) {
if (!$Row['active']) $Row['name'] .= ' (Ожидает модерации)';

 echo '<div role="tabpanel" class="tab-pane" id="comments">
                <ul class="spost_nav">
                  <li>
                    <div class="media wow fadeInDown"> <a href="/loads/material/id/'.$Row['id'].'" class="media-left">  <img src="/catalog/img/'.$Row['dimg'].'/'.$Row['id'].'.jpg"> </a>
                      <div class="media-body"> <a href="/loads/material/id/'.$Row['id'].'">'.$Row['name'].'</a> 
                      <div class="post_commentbox"> <a href="#"><i class="fa fa-user"></i>'.$Row['added'].' </a> <span><i class="fa fa-calendar"></i>'.$Row['date'].'</span> <a href="#"><i class="fa fa-tags"></i>About</a> <a href="#"><i class="fa fa-thumbs-o-up"></i>'.$Row['rate'].'</a> </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>';
}
?> 
             <?php  include("page/including.php");
            ?>
  </section>
<?php Footer() ?>
</div>
</body>
</html>